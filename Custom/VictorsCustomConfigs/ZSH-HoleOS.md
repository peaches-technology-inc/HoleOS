## Installing ZSH in HoleOS

Open the Terminal [CTRL-ALT-T]

Then, type the following commands: `sudo apt-get update` and `sudo apt-get -y install zsh`

After downloading, ZSH should be installed.

To check if it worked or not type the following: `zsh --version` and you should see this:

![ZSH Properly installed.](zsh_version_screen.png)

## Setting ZSH as the default shell

Open the Terminal [CTRL-ALT-T]

Then, In the Terminal window, type: `chsh -s /bin/zsh` [You will be asked for your password!]

Afterwards, logout and the log back in.

Thats it!, keep in mind that after logging back in and opening terminal, you will still have to configure ZSH, which is pretty easy!

Stay Safe!

## Setting Bash back as the default shell

Open a terminal window [CTRL-ALT-T]

Just like before run this:`chsh -s /bin/bash` [You will be asked for your password!]

Then, Log out and Log back in.

Thats it!
