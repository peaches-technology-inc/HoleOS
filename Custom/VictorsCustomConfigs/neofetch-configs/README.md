## Custom neofetch configs

Hey, these are my custom neofetch configs!

![T430s neofetch config.](thinkpad-t430s-neofetchconfig.png)

The config you just saw is `victor-laptop-neofetch.conf`

It was made to be used in my Thinkpad T430s, but I assume it should work in any laptop.

## Installing the custom neofetch configs

To install these configs, first of all, you will want to download them.

After downloading them, you'd want to rename the one you want to use to `config.conf`

After that, copy it to `~/.config/neofetch` [Keep in mind this is a hidden folder, to see it, you might need to use CTRL + H]

TODO:

- Add more configs
- Add original neofetch config [from benjike]
